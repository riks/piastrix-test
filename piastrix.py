import json
import hashlib
import logging

from urllib.request import Request, urlopen

from flask import render_template, redirect


class PiastrixError(Exception):
    """
    Piastrix API error
    """

    pass


class Piastrix:
    """
    Piastrix API connector
    """

    CURRENCIES = {
        "EUR": 978,
        "USD": 840,
        "RUB": 643,
    }

    _PAY_ENDPOINT = "https://pay.piastrix.com/ru/pay"
    _BILL_ENDPOINT = "https://core.piastrix.com/bill/create"
    _INVOICE_ENDPOINT = "https://core.piastrix.com/invoice/create"

    def __init__(self, secret, shop_id):
        """
        Initializes API with shop ID and secret key
        """

        self._secret = secret or ""
        self._shop_id = shop_id

        self._logger = logging.getLogger(__name__)

    def pay(self, shop_order_id, currency, amount, description, **kwargs):
        """
        Performs Pay flow
        """

        amount = "{0:.2f}".format(float(amount))

        sign = self._sign(shop_id=self._shop_id,
                          shop_order_id=shop_order_id, currency=currency,
                          amount=amount)

        form = {
            "shop_id": self._shop_id,
            "shop_order_id": shop_order_id,
            "currency": currency,
            "amount": amount,
            "description": description,
            "sign": sign,
        }

        form.update(kwargs)

        self._logger.debug("Pay %r data: %s",
                           shop_order_id, form)

        return render_template("piastrix_pay.html",
                               action=self._PAY_ENDPOINT, form=form)

    def bill(self, shop_order_id, currency, amount, description, **kwargs):
        """
        Performs Bill flow
        """

        amount = "{0:.2f}".format(float(amount))

        sign = self._sign(shop_id=self._shop_id,
                          shop_order_id=shop_order_id, shop_currency=currency,
                          shop_amount=amount, payer_currency=currency)

        data = {
            "payer_currency": currency,
            "shop_amount": amount,
            "shop_currency": currency,
            "shop_id": self._shop_id,
            "shop_order_id": shop_order_id,
            "description": description,
            "sign": sign,
        }

        request = Request(self._BILL_ENDPOINT,
                          json.dumps(data).encode("utf-8"),
                          headers={"Content-Type": "application/json"},
                          method="POST")

        with urlopen(request) as response:
            data = json.loads(response.read())

            if not data["result"]:
                self._logger.error("Bill %r error: %s",
                                   shop_order_id, data)

                raise PiastrixError(data["message"])

            self._logger.debug("Bill %r response: %s",
                               shop_order_id, data)

            data = data["data"]

        return redirect(data["url"])

    def invoice(self, shop_order_id, currency, amount, description, payway,
                **kwargs):
        """
        Performs Invoice flow
        """

        amount = "{0:.2f}".format(float(amount))

        sign = self._sign(shop_id=self._shop_id,
                          shop_order_id=shop_order_id, currency=currency,
                          amount=amount, payway=payway)

        data = {
            "currency": currency,
            "amount": amount,
            "shop_id": self._shop_id,
            "shop_order_id": shop_order_id,
            "description": description,
            "payway": payway,
            "sign": sign,
        }

        request = Request(self._INVOICE_ENDPOINT,
                          json.dumps(data).encode("utf-8"),
                          headers={"Content-Type": "application/json"},
                          method="POST")

        with urlopen(request) as response:
            data = json.loads(response.read())

            if not data["result"]:
                self._logger.error("Invoice %r error: %s",
                                   shop_order_id, data)

                raise PiastrixError(data["message"])

            self._logger.debug("Invoice %r response: %s",
                               shop_order_id, data)

            data = data["data"]

        return render_template("piastrix_invoice.html", method=data["method"],
                               action=data["url"], form=data["data"])

    def _sign(self, **kwargs):
        """
        API signing routine
        """

        data = []

        for key in sorted(kwargs.keys()):
            data.append(str(kwargs[key]))

        hasher = hashlib.sha256()

        hasher.update((":".join(data) + self._secret).encode("utf-8"))

        return hasher.hexdigest()
