CREATE TABLE IF NOT EXISTS public.payment
(
  id bigserial NOT NULL,
  currency numeric(3,0) NOT NULL,
  amount numeric(10,2) NOT NULL,
  description text NOT NULL,
  created_at timestamp without time zone NOT NULL,
  CONSTRAINT payment_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);