import os

import psycopg2
from psycopg2.extensions import parse_dsn

dsn = parse_dsn(os.environ["DATABASE_URL"])

db = psycopg2.connect(**dsn)


def save_payment(currency, amount, description):
    """
    Saves payment into database
    """

    with db:
        with db.cursor() as cursor:
            cursor.execute(
                """INSERT INTO "payment" (
            "currency",
            "amount",
            "description",
            "created_at"
        ) VALUES (
            %s,
            %s,
            %s,
            "timezone"('UTC', "now"())
        ) RETURNING "id";""",
                (currency, amount, description or "",),
            )

            (order_id,) = cursor.fetchone()

    return order_id
