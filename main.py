import os
import logging

from flask import Flask, request, render_template, url_for, redirect, flash

from piastrix import Piastrix, PiastrixError

app = Flask(__name__)

app.config.from_mapping({
    "SECRET_KEY": os.environ.get("FLASK_SECRET_KEY", None),
})

application = app

piastrix = Piastrix(os.environ["PIASTRIX_KEY"], int(os.environ["PIASTRIX_ID"]))

pay_methods = {
    978: lambda order_id, currency, amount, description:
        piastrix.pay(order_id, currency, amount, description),
    840: lambda order_id, currency, amount, description:
        piastrix.bill(order_id, currency, amount, description),
    643: lambda order_id, currency, amount, description:
        piastrix.invoice(order_id, currency, amount, description, "payeer_rub")
}


@app.route("/", methods=["GET"])
def index():
    return render_template("main.html")


@app.route("/pay", methods=["POST"])
def pay():
    # Dirty singleton
    import storage

    currency = Piastrix.CURRENCIES.get(request.form["currency"].upper(), None)
    amount = float(request.form["amount"])
    description = request.form["description"]

    if currency is None:
        flash("Invalid currency")

        return redirect(url_for("index"))

    logger = logging.getLogger(__name__)

    order_id = storage.save_payment(currency, amount, description)

    logger.info("Payment %r saved", order_id)

    try:
        return pay_methods[currency](order_id, currency, amount, description)
    except PiastrixError as error:
        logger.exception("Payment %r error", order_id)

        flash(str(error))

        return redirect(url_for("index"))
